import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Item extends Component{
    render() {
        return (
            <div className="col-md-3">
                <div style={{backgroundColor: '#107180'}} className="card card-pricing" data-color={this.props.color}>
                    <div className="card-body">
                        <div className="card-icon">
                            <span className="icon-simple"><Link to={'/car?id='+this.props.id}><img alt={'Car'} src={this.props.image} /></Link></span>
                        </div>
                        <h3 className="card-title" >{this.props.name}</h3>
                        <h4 className="card-title" >{this.props.cost}	&#36;</h4>

                        <p className="card-description">
                            {this.props.description}
                        </p>
                        <div className="card-footer">
                            <Link to={'/car?id='+this.props.id} className="btn btn-neutral btn-round">Подробнее</Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
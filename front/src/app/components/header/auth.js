import React, {Component} from 'react';
import '../../main/style.css';
import { Link } from 'react-router-dom';

export default class Auth extends Component {
    constructor(props) {
        super(props);

        this.state = {
            balance: null
        }
    }

    render() {
        return (
            <ul className="nav navbar-nav flex-row justify-content-between ml-auto">
                <li className="dropdown order-1">
                    <p style={{cursor: 'default'}} className="btn btn-neutral btn-round">Ваш баланс: {this.state.balance} 	&#36;</p>
                </li>
                <li className="dropdown order-2">
                    <Link to='/personal_area' className="btn btn-info btn-round">Личный кабинет</Link>
                </li>
                <li className="dropdown order-2">
                    <button onClick={this.exit.bind(this)} className="btn  login_btn">Выход</button>
                </li>
            </ul>
        )
    }

    componentWillMount(){
        // Update Balance
        setInterval(function () {
            try {
                const xhr = new XMLHttpRequest();

                const that = this;
                xhr.open('GET', `http://localhost:5000/get_user_balance?id=${JSON.parse(window.localStorage.getItem('tokenDecode'))._id}`);

                xhr.onload = function () {
                    if (this.status === 200) {
                        that.setState({balance: this.response});
                    }
                };
                xhr.send();
            } catch (err) {
                console.log(err);
            }
        }.bind(this), 1000)
    }

    exit(event){
        window.localStorage.clear('token');
        window.localStorage.clear('tokenDecode');
        document.cookie = 'token=';
        this.props.handler(event);
    }
}
const passport = require('passport');
const JwtStrategy = require("passport-jwt").Strategy;

const User = require('../db/User');

module.exports.jwt = function () {
    const jwtOptions = require('../libs/config').jwt;

    passport.use(new JwtStrategy(jwtOptions, function (payload, done) {
            User.findById(payload._id, (err, user) => {
                if (err) {
                    return done(err);
                }
                if (user) {
                    done(null, user);
                } else {
                    done(null, false);
                }
            });
        })
    );
};

module.exports.middleware = function (app) {
    app.use(passport.initialize());
};
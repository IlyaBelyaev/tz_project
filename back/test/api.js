//During the test the env variable is set to test
//process.env.NODE_ENV = 'test';
const fs = require('fs');
const jwt = require('jsonwebtoken');

let Lots = require('../db/Lots');
let Cars = require('../db/Cars');
let User = require('../db/User');

//Подключаем dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

chai.use(chaiHttp);



//Наш основной блок
describe('API Авторизации', () => {
    //Перед каждым тестом чистим базу
    before((done) => {
        User.remove({}, (err) => {
            if (err) console.log(err);

            done();
        });
    });

    let token;

    // Sign_up test
    describe('POST sign_up', () => {
        it('РЕГИСТРАЦИЯ. Должно возвращять "OK"', (done) => {
            const data = {
                name: 'name2',
                email: 'email2@gmail.com',
                password: '12345678'
            };
            chai.request(server)
                .post('/sign_up')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .send(`name=${encodeURIComponent(data.name)}&email=${encodeURIComponent(data.email)}&password=${encodeURIComponent(data.password)}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.text.should.be.eql('OK');
                    done();
                });
        });
        // TODO it на воод пароля, почты и имени
    });

    describe('POST sign_in', () => {
        it('ВХОД. Корректные данные. Должен возвращять токен', (done) => {
            const data = {
                email: 'email2@gmail.com',
                password: '12345678'
            };
            chai.request(server)
                .post('/sign_in')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .send(`email=${encodeURIComponent(data.email)}&password=${encodeURIComponent(data.password)}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    chai.expect(res.body).to.have.property('token');
                    done();
                });
        });
        it('ВХОД. Некорректная почта.', (done) => {
            const data = {
                email: '123',
                password: '12345678'
            };
            chai.request(server)
                .post('/sign_in')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .send(`email=${encodeURIComponent(data.email)}&password=${encodeURIComponent(data.password)}`)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.text.should.be.eql('Такая электронная почта не зарегистрирована!');
                    done();
                });
        });
        it('ВХОД. Некорректный пароль.', (done) => {
            const data = {
                email: 'email2@gmail.com',
                password: '123'
            };
            chai.request(server)
                .post('/sign_in')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .send(`email=${encodeURIComponent(data.email)}&password=${encodeURIComponent(data.password)}`)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.text.should.be.eql('Неверный пароль!');
                    done();
                });
        });
    });
});

describe('API', () => {

    let car_first_id;
    let car_second_id;
    let car_first_image;
    let first_token;
    let second_token;

    before((done) => {
        Lots.remove({}, (err) => {
            if (err) console.log(err);
            done();
        });
    });

    before((done) => {
        Cars.remove({}, (err) => {
            if (err) console.log(err);
            done();
        });
    });

    before((done) => {
        User.remove({}, (err) => {
            if (err) console.log(err);
            done();
        });
    });

    // Регистрация юзера 1
    before((done) => {
        const data = {
            name: 'name',
            email: 'email@gmail.com',
            password: '12345678'
        };
        chai.request(server)
            .post('/sign_up')
            .set('Content-Type', 'application/x-www-form-urlencoded')
            .send(`name=${encodeURIComponent(data.name)}&email=${encodeURIComponent(data.email)}&password=${encodeURIComponent(data.password)}`)
            .end((err, res) => {
                if (err) console.log(err);
                res.should.have.status(200);
                res.text.should.be.eql('OK');
                done();
            });
    });

    // Регистрация юзера 2
    before((done) => {
        const data = {
            name: 'name2',
            email: 'email2@gmail.com',
            password: '12345678'
        };
        chai.request(server)
            .post('/sign_up')
            .set('Content-Type', 'application/x-www-form-urlencoded')
            .send(`name=${encodeURIComponent(data.name)}&email=${encodeURIComponent(data.email)}&password=${encodeURIComponent(data.password)}`)
            .end((err, res) => {
                if (err) console.log(err);
                res.should.have.status(200);
                res.text.should.be.eql('OK');
                done();
            });
    });

    // Получение токена первого юзера
    before((done) => {
        const data = {
            email: 'email@gmail.com',
            password: '12345678'
        };
        chai.request(server)
            .post('/sign_in')
            .set('Content-Type', 'application/x-www-form-urlencoded')
            .send(`email=${encodeURIComponent(data.email)}&password=${encodeURIComponent(data.password)}`)
            .end((err, res) => {
                if (err) console.log(err);
                res.should.have.status(200);
                res.body.should.be.a('object');
                chai.expect(res.body).to.have.property('token');
                first_token = res.body.token;
                done();
            });
    });

    // Получение токена второго юзера
    before((done) => {
        const data = {
            email: 'email2@gmail.com',
            password: '12345678'
        };
        chai.request(server)
            .post('/sign_in')
            .set('Content-Type', 'application/x-www-form-urlencoded')
            .send(`email=${encodeURIComponent(data.email)}&password=${encodeURIComponent(data.password)}`)
            .end((err, res) => {
                if (err) console.log(err);
                res.should.have.status(200);
                res.body.should.be.a('object');
                chai.expect(res.body).to.have.property('token');
                second_token = res.body.token;
                done();
            });
    });

    describe('POST add_car', () => {
        it('Добавления авто в коллекцию первого юзера. Должно возвращять объект с информацией о добавленном авто', (done) => {
            const img = fs.readFileSync(__dirname + '/test.jpg');
            console.log(img);
            chai.request(server)
                .post('/add_car')
                .set('Cookie', 'token='+first_token)
                .attach('image', img, 'test.jpg')
                .field('name', 'volga')
                .field('id_user', jwt.decode(first_token)._id)
                .end((err, res) => {
                    if (err) console.log(err);

                    console.log(res);

                    car_first_id = res.body._id;
                    car_first_image = res.body.image;

                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    chai.expect(res.body).to.have.property('selling');
                    chai.expect(res.body).to.have.property('owner_id');
                    chai.expect(res.body).to.have.property('car_name');
                    chai.expect(res.body).to.have.property('image');
                    chai.expect(res.body).to.have.property('_id');
                    done();
                });
        });
        it('Добавление авто в коллекцию. Без картинки. Ошибка.', (done) => {
            chai.request(server)
                .post('/add_car')
                .set('Cookie', 'token='+first_token)
                .field('name', 'volga')
                .field('id_user', jwt.decode(first_token)._id)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.text.should.be.eql('Нет фотографии');
                    done();
                });
        });
        it('Добавление авто второму юзеру', (done) => {
            chai.request(server)
                .post('/add_car')
                .set('Cookie', 'token='+second_token)
                .attach('image', fs.readFileSync(__dirname + '/test.jpg'), 'test.jpg')
                .field('name', 'GAS')
                .field('id_user', jwt.decode(second_token)._id)
                .end((err, res) => {
                    car_second_id = res.body._id;

                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    chai.expect(res.body).to.have.property('selling');
                    chai.expect(res.body).to.have.property('owner_id');
                    chai.expect(res.body).to.have.property('car_name');
                    chai.expect(res.body).to.have.property('image');
                    chai.expect(res.body).to.have.property('_id');
                    done();
                });
        });
    });

    describe('POST sell_car', () => {

        it('Продажа авто первого юзера за 10', (done) => {
            chai.request(server)
                .post('/sell_car')
                .set('Cookie', 'token='+first_token)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .type('form')
                .send(`id_car=${car_first_id}`)
                .send(`cost=10`)
                .send(`description=Volga`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    chai.expect(res.body).to.have.property('bets');
                    chai.expect(res.body).to.have.property('owner_id');
                    chai.expect(res.body).to.have.property('cost');
                    chai.expect(res.body).to.have.property('description');
                    chai.expect(res.body).to.have.property('car_id');
                    chai.expect(res.body).to.have.property('car_name');
                    chai.expect(res.body).to.have.property('image');
                    chai.expect(res.body).to.have.property('_id');
                    done();
                });
        });

        it('Продажа авто второго юзера за 1000', (done) => {
            chai.request(server)
                .post('/sell_car')
                .set('Cookie', 'token='+second_token)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .type('form')
                .send(`id_car=${car_second_id}`)
                .send(`cost=1000`)
                .send(`description=Volga`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    chai.expect(res.body).to.have.property('bets');
                    chai.expect(res.body).to.have.property('owner_id');
                    chai.expect(res.body).to.have.property('cost');
                    chai.expect(res.body).to.have.property('description');
                    chai.expect(res.body).to.have.property('car_id');
                    chai.expect(res.body).to.have.property('car_name');
                    chai.expect(res.body).to.have.property('image');
                    chai.expect(res.body).to.have.property('_id');
                    done();
                });
        });

        it('Попытка выставить на продажу автомобиль, который уже продается', (done) => {
            chai.request(server)
                .post('/sell_car')
                .set('Cookie', 'token='+first_token)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .type('form')
                .send(`id_car=${car_first_id}`)
                .send(`cost=100`)
                .send(`description=Volga`)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.text.should.be.eql('Машина уже на продаже');
                    done();
                });
        });
    });

    describe('POST buy_car', () => {

        it('Покупка автомобиля вторым пользователем', (done) => {
            chai.request(server)
                .post('/buy_car')
                .set('Cookie', 'token='+second_token)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .type('form')
                .send(`id_car=${car_first_id}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.text.should.be.eql('OK');
                    done();
                });
        });

        it('Покупка авто первым юзером, при недостатке средст на счете', (done) => {
            chai.request(server)
                .post('/buy_car')
                .set('Cookie', 'token='+first_token)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .type('form')
                .send(`id_car=${car_second_id}`)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.text.should.be.eql('Недостаточно денег');
                    done();
                });
        });

        it('Попытка купить свой же автомобиль', (done) => {
            chai.request(server)
                .post('/buy_car')
                .set('Cookie', 'token='+second_token)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .type('form')
                .send(`id_car=${car_second_id}`)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.text.should.be.eql('Вы не можете купить свою машину');
                    done();
                });
        });

    });

    describe('GET get_bets_by_id', () => {
        it('Получение ставок на автомобиль, по параметру ID машины', (done) => {
            chai.request(server)
                .get('/get_bets_by_id')
                .query({id: car_first_id})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    chai.expect(res.body[0]).to.have.property('user_id');
                    chai.expect(res.body[0]).to.have.property('user_name');
                    chai.expect(res.body[0]).to.have.property('bet');
                    done();
                });
        });

        it('Неверный параметр ID', (done) => {
            const id = '123';
            chai.request(server)
                .get('/get_bets_by_id')
                .query({id: id})
                .end((err, res) => {
                    res.should.have.status(400);
                    res.text.should.be.eql(`Cast to ObjectId failed for value "${id}" at path "car_id" for model "Lots"`);
                    done();
                });
        });
    });

    describe('GET get_lot_by_id', () => {
        it('Получение всей информации о лоте по ID авто', (done) => {
            chai.request(server)
                .get('/get_lot_by_id')
                .query({id: car_first_id})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    chai.expect(res.body).to.have.property('bets');
                    chai.expect(res.body).to.have.property('owner_id');
                    chai.expect(res.body).to.have.property('cost');
                    chai.expect(res.body).to.have.property('description');
                    chai.expect(res.body).to.have.property('car_id');
                    chai.expect(res.body).to.have.property('car_name');
                    chai.expect(res.body).to.have.property('image');
                    chai.expect(res.body).to.have.property('endTime');
                    done();
                });
        });

        it('Неверный параметр ID', (done) => {
            const id = '123';
            chai.request(server)
                .get('/get_lot_by_id')
                .query({id: id})
                .end((err, res) => {
                    res.should.have.status(400);
                    res.text.should.be.eql(`Cast to ObjectId failed for value "${id}" at path "car_id" for model "Lots"`);
                    done();
                });
        });
    });

    describe('GET get_lots', () => {
        it('Получение последних лотов, количество в параметре count', (done) => {
            chai.request(server)
                .get('/get_lots')
                .query({count: '4'})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                });
        });
    });

    describe('GET get_image', () => {

        it('Получение фотки авто если id правильный', (done) => {
            const id_car = car_first_image.split('id=')[1];
            const expected = fs.readFileSync('./test/test.jpg');
            chai.request(server)
                .get('/image')
                .query({id: id_car})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.deep.equal(expected);
                    done();
                });
        });

        it('Получение фотки авто если id НЕ правильный', (done) => {
            const id_car = car_first_image.split('id=')[1];

            chai.request(server)
                .get('/image')
                .query({id: id_car+'123'})
                .end((err, res) => {
                    res.should.have.status(400);
                    res.text.should.be.eql('Not found');
                    done();
                });
        });
    });

    describe('GET get_user_balance', () => {

        it('Получение баланса юзера по его ID', (done) => {
            chai.request(server)
                .get('/get_user_balance')
                .query({id: jwt.decode(first_token)._id})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.text.should.be.a('string');
                    done();
                });
        });

        it('Неверный ID', (done) => {
            const id = '5af2a110d9ehc0284489882f';
            chai.request(server)
                .get('/get_user_balance')
                .query({id: id})
                .end((err, res) => {
                    res.should.have.status(400);
                    res.text.should.be.a('string');
                    res.text.should.be.eql(`Cast to ObjectId failed for value "${id}" at path "_id" for model "User"`);
                    done();
                });
        });
    });

    describe('GET get_user_cars', () => {

        it('Получение машин юзера по его ID', (done) => {
            chai.request(server)
                .get('/get_user_cars')
                .query({id: jwt.decode(first_token)._id})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    chai.expect(res.body[0]).to.have.property('_id');
                    chai.expect(res.body[0]).to.have.property('selling');
                    chai.expect(res.body[0]).to.have.property('owner_id');
                    chai.expect(res.body[0]).to.have.property('car_name');
                    chai.expect(res.body[0]).to.have.property('image');
                    done();
                });
        });

        it('Неверный ID', (done) => {
            const id = '5af2a110d9ehc0284489882f';
            chai.request(server)
                .get('/get_user_cars')
                .query({id: id})
                .end((err, res) => {
                    res.should.have.status(400);
                    res.text.should.be.a('string');
                    res.text.should.be.eql(`Cast to ObjectId failed for value "${id}" at path "owner_id" for model "Cars"`);
                    done();
                });
        });
    });
});
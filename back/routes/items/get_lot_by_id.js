const Lots = require('../../db/Lots');

// OK
module.exports = function (req, res) {
    Lots.findOne({car_id: req.query.id})
        .then(result => res.send(result))
        .catch(err => {
            res.header('Content-Type', 'text/plain');
            res.status(400).send(err.message)
        });
};
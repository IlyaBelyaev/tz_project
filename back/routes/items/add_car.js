const Car = require('../../db/Cars');
const mongoose = require('mongoose');

// OK
module.exports = async function (req, res) {
    if (req.file === undefined){
        res.header('Content-Type', 'text/plain');
        res.status(400).send('Нет фотографии');
    } else {
        const id_user = req.body.id_user;
        const name = req.body.name;
        const image = 'http://' + req.headers.host + '/image?id=' + req.file.filename;

        const newCar = new Car({
            owner_id: id_user,
            car_name: name,
            image: image,
        });

        newCar.save()
            .then(result => res.send(result))
            .catch(err => {
                res.status(500).send(err.message);
                console.log(err)
            });
    }
};
const Car = require('../../db/Cars');
const User = require('../../db/User');
const Lots = require('../../db/Lots');
const config = require('../../libs/config');
const jwt = require('jsonwebtoken');

module.exports = function (req, res) {
    const id_car = req.body.id_car;
    const user_id = jwt.decode(req.cookies.token)._id;
    // Ищем машину по ID
    Lots.findOne({car_id: id_car})
        .then(result => {
            const car_cost = result.cost;
            // Проверка на 2 минуты
            if (result.endTime === undefined || new Date().getTime() < result.endTime) {

                // Проверка на покупку своего же автомобиля
                if (result.owner_id.toString() !== user_id.toString()) {

                User.findById(user_id)
                    .then(result => {
                        const user_balance = result.cash;
                        const user_name = result.name;

                        // Если хватает денег на машину
                        if (user_balance >= car_cost) {
                            // Новая цена машины,ставка на 15% больше
                            const newCarPrice = Math.round(car_cost + car_cost / 100 * 15);
                            const newBalance = user_balance - car_cost;

                            // Списываем деньги у покупателя
                            User.update({_id: user_id}, {cash: newBalance}, function (err) {
                                if (err) console.log(err);
                                res.header('Content-Type', 'text/plain');
                                res.status(200).send('OK');
                            });

                            // Добавляем ставку и обновляем цену
                            Lots.findOneAndUpdate({car_id: id_car}, {
                                $push: {
                                    bets: {
                                        user_id: user_id,
                                        user_name: user_name,
                                        bet: car_cost
                                    }
                                }, cost: newCarPrice, endTime: new Date().getTime() + config.TIMER_AUCT
                            }, function (err, result) {
                                if (err) {
                                    console.log(err);
                                } else
                                // Если есть предыдущие ставки, то возвращяем деньги
                                if (result.bets.length !== 0) {
                                    const lastBet = result.bets[result.bets.length - 1].bet;
                                    User.update({_id: user_id}, {$inc: {cash: lastBet}}, function (err) {
                                        if (err) console.log(err);
                                    })
                                }
                            })
                        } else {
                            res.header('Content-Type', 'text/plain');
                            res.status(400).send('Недостаточно денег')
                        }
                    })
                } else {
                    res.header('Content-Type', 'text/plain');
                    res.status(400).send('Вы не можете купить свою машину')
                }
            } else {
                const winner_id = result.bets[result.bets.length-1].user_id;
                const last_bet = result.bets[result.bets.length-1].bet;

                // Добавляем деньги продавцу
                User.update({_id: result.owner_id}, {$inc: {cash: last_bet}}, function (err) {
                    if (err) console.log(err);
                });

                // Меняем владельца авто
                Car.update({_id: result.car_id}, {selling: false, owner_id: winner_id}, function (err) {
                    if (err) console.log(err);
                });

                // Удаляем авто из лотов
                Lots.findOneAndRemove({car_id: id_car}, function (err) {
                    if (err) console.log(err);
                })
            }
        })
        .catch(err => {
            console.log(err);
            res.header('Content-Type', 'text/plain');
            res.status(400).send(err.message)
        });
};


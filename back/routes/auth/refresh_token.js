'use strict';
const User = require('../../db/User');
const jwt = require('jsonwebtoken');
const config = require('../../libs/config');
const log = require('../../libs/log')(module);

module.exports =  function (refreshToken) {
    return User.findOne({refreshToken: refreshToken})
        .then(payload => {
            // REFRESH TOKEN
            const refreshToken = jwt.sign({type: 'refresh'}, config.jwt.secretOrKey, { expiresIn: config.jwt.expiresInRefreshToken });
            User.update({ email: payload.email }, { $set: {refreshToken: refreshToken} }, (err) => {
                if (err) log.error(err);
            });

            // ACCESS TOKEN
            const tokenInfo = {
                _id: payload._id,
                name: payload.name,
                email: payload.email,
                cash: payload.cash,
                refreshToken: refreshToken
            };

            // Шифруем
            return jwt.sign(tokenInfo, config.jwt.secretOrKey, {expiresIn: config.jwt.expiresIn});
        })
        .catch(err => new Error('Пользователя с таким токеном не существует!'));
};
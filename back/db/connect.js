const mongoose = require('mongoose');
const config = require('../libs/config').mongoose;

mongoose.connect(config.urlDocker)
    .catch(err => {
        mongoose.connect(config.url);
    });

module.exports = mongoose.connection;